**INTRODUCTION**

OVERSCAN is the extension of Burp Suite which compatible with all edition (Community,
Professional, and Enterprise). The extension will analyze the traffic from Burp Proxy to scan the
vulnerability of missing important parameter to against the threat. The parameters for scanning are
based on RFC 6819 “OAuth 2.0 Threat Model and Security Considerations”. The final result of scanning 
will be presented as a PDF report which include the vulnerability information, recommendation, 
and penetration method. The possible threats that would be happened when missing some parameters 
that could be detected by OVERSCAN are stated as follow.

| **Missing Parameters** | **Description** |
| ------ | ------ |
| Client_ID | The Client_Id is a unique identifier that provided to every client application. It must not be guestable by any third parties. If the Client_Id can be guestable, the attacker could exploit it using phishing attacks to utilize it for accessing the protected resources, which could lead to information leakage. Therefor, this parameter should be included in every request to validate the client for preventing Authorization "code" Leakage through Counterfeit Client and Code Substitution (OAuth Login) as stated in the RFC 6819. |
| CSRF-Token | CSRF-Token or XSRF-Token is a uniquely random HTTP token to prevent against CSRF attack. This token is unique for each user session and large random value. CSRF Token should bound to the user agent and pass in the state parameter to the authorization server to prevent CSRF Attack against redirect-URI as stated in the RFC 6819. | 
| State-Parameter | State-Parameter is used for CSRF attack mitigation on the redirection endpoint. This parameter allows the user or client to restore the previous state of the application. The value of this parameter is unique and nonguestable that provided in the initial request to validate the response by confirming if the value from the response is match to the initial request. Therefore, the state parameter should be included in OAuth 2.0 protocol to link the authorization with redirect-uri that used to transmit the access token, which can prevent CSRF Attack against redirect-uri and DoS Using Manufactured Authorization "codes" as stated in the RFC 6819. |
| SSL/TLS (HTTPS for Redirect URI) | The redirect_uri is a parameter to change the direction of the traffic for the next endpoint of a flow. The URI of all endpoints should be integrated the SSL/TLS protection which is HTTPS to prevent Authorization "code" Phishing and User Session Impersonation as stated in RFC 6819. | 
| XSS-Token | The XSS protection is a response header in a web browser that stops a webpage from loading when cross-site scripting (XSS) attacks are detected. There are four types of syntax to configure the protection. The first is X-XSSProtection: 0 which means disable XSS filtering. The second is X-XSSProtection: 1 which means that it enables XSS filtering, the browser will sanitize the page if cross-site script attack is detected. The third is X-XSSProtection: 1; mode=block which means that XSS filter is enabled and the browser will prevent the page from rendering if the attack is detected. The last is X-XSS-Protection: 1; report=[reporting-uri] which means that XSS filter is enabled and the browser will sanitize the page and report the violation to the specific URI. This protection can prevent Obtaining Authorization "codes" from Authorization Server Database as stated in RFC 6819. |
| MIME Sniffing | MIME Sniffing is a technique used by a web browser to do examining the content of the asset by determining a file format of the asset. However, MIME Sniffing can cause vulnerability when the attacker leverage MIME Sniffing to send an XSS attack. The prevention is providing X-Content-Type-Options header that can disable MIME Sniffing function by configuring the value to “nosniff”. This prevention can against the Obtaining Authorization "codes" from Authorization Server Database as stated in the RFC 6819. |
| X-Frame Options Header | X-Frame-Options is an HTTP security header to provides clickjacking protection under the iFrames. The options consist of three configurations. The first is X-Frame-Options: DENY which means completely disable the loading of the page in a frame. The second is X-Frame-Options: SAMEORIGIN which allows the page to be loaded in a frame on the same origin as the page itself only. The last option is X-Frame-Options: ALLOW-FROM which allows the page to be loaded only in a frame on the specific origin or domain. These options header can prevent Clickjacking Attack against Authorization as stated in the RFC 6819. |


**Burp Suite Installation and Configuration:**

1.  Download Burp Suite Community Edition: [https://portswigger.net/burp/communitydownload](url)

![alt text](image/testwithgit.png)

2.  Configure proxy on Burp Suite.

![alt text](image/readme_A13.png)

3.  Configure proxy on web browser (ex. Mozilla Firefox).

![alt text](image/readme_A14.png)

![alt text](image/readme_A15.png)

![alt text](image/readme_A16.png)

4.  Turn off proxy interception on Burp Suite program.

![alt text](image/readme_A17.png)

**Adding OVERSCAN extension into Burp Suite program:**

1.  Go to tab "Extender" --> "Extensions"

![alt text](image/readme_A19.png)

2.  Click "Add"

![alt text](image/readme_A20.png)

3.  Select OVERSCAN.jar file and click "Open"

![alt text](image/readme_A21.png)

![alt text](image/readme_A22.jpg)

**How to Use OVERSCAN:**

1.  Login with Third-Party authorization, OVERSCAN will clasify all traffics that related to OAuth 2.0 protocol.
2.  All results of scanning will be displayed on OVERSCAN tab in a Burp Suite as a table.

![alt text](image/howto1.JPG)


3.  The raw result of scanning will be exported as TXT file stored in the Burp Suite file location named "Result" folder.

![alt text](image/howto2.JPG)

4.  Use executeable JAR file named J2HTML_JAVA.jar to select TXT file and transform into an appropriate PDF file format.

![alt text](image/howto3.JPG)

5.  The PDF report will be stored in "Report" folder in the same directory as the executeable JAR file named J2HTML_JAVA.jar

![alt text](image/howto4.JPG)

