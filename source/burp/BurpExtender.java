
package burp;

import GUI.UITab;
import GUI.Logging;
import java.io.PrintWriter;

import Scan.ScanAndMarkSSO;



public class BurpExtender implements IBurpExtender, IExtensionStateListener {
    
    public static final String EXTENSION_NAME = "Overscan";
    private UITab tab;
    private static PrintWriter stdout;
    private static PrintWriter stderr;
    
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {
        
        // set our extension name
        callbacks.setExtensionName(EXTENSION_NAME);
        stdout = new PrintWriter(callbacks.getStdout(), true);
        stderr = new PrintWriter(callbacks.getStderr(), true);
        
        tab = new UITab(callbacks);
    
       /* LocalTime t = LocalTime.now();     TIME 
        String time = t.toString().substring(0, t.toString().length()-4);*/
       // callbacks.addSuiteTab(this);
        //integrate the extension of Christian Mainka
        final ScanAndMarkSSO scanAndMark = new ScanAndMarkSSO(callbacks);
	callbacks.registerHttpListener(scanAndMark);
        Logging.getInstance().log(getClass(), "Scanner registered.", Logging.INFO);
        
        callbacks.registerExtensionStateListener((IExtensionStateListener) this);
        Logging.getInstance().log(getClass(), "ExtensionStateListener registered", Logging.INFO);
        
        //register Intruder payload generator
     //   callbacks.registerIntruderPayloadGeneratorFactory(new DTDPayloadFactory(callbacks));
        
        //Start logging
        Logging.getInstance().log(getClass(), "Init. complete.", Logging.INFO);
        
       
    }
   public void extensionUnloaded() {
        Logging.getInstance().log(getClass(), "Extension is now unloaded.", Logging.INFO);
        stdout.println("");
        stderr.println("");
    }
   public static PrintWriter getStdOut(){
        return stdout;
    }
    
    public static PrintWriter getStdErr(){
        return stderr;
    }
 
}


   
    
