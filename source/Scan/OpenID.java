
package Scan;

import burp.IBurpExtenderCallbacks;
import burp.IHttpRequestResponse;
import burp.IParameter;
import burp.IRequestInfo;
import GUI.Logging;
import table.SSOProtocol;
import static table.SSOProtocol.getIDOfLastList;    
import static table.SSOProtocol.newProtocolflowID;
import java.util.ArrayList;
import java.util.List;


public class OpenID extends SSOProtocol{

    public static final String OPENID_V1 = "OpenID v1.0";


    public static final String OPENID_PARAM = "openid.mode";


    public static final String OPENID_REQUEST = "checkid_setup";


    public static final String OPENID_RESPONSE = "id_res";


    public static final String ID = "openid.identity";


    public static final String OPENID_V2 = "OpenID v2.0";
    
    private String return_to = "";
    

    public OpenID(IHttpRequestResponse message, String protocol, IBurpExtenderCallbacks callbacks){
        super(message, protocol, callbacks);
        super.setToken(findToken());
        super.setProtocolflowID(analyseProtocol());
        add(this, getProtocolflowID());
    }

    @Override
    public String decode(String input) {
        if(Encoding.isURLEncoded(input)){
            return super.getCallbacks().getHelpers().urlDecode(input);
        }
        return input;
    }


    @Override
    public String findToken() {
          IRequestInfo iri = super.getCallbacks().getHelpers().analyzeRequest(super.getMessage());
          List<IParameter> list = iri.getParameters();
          String id = "Not Found!";
          for(IParameter p : list){
              if(p.getName().equals("openid.identity")){
                  id = decode(p.getValue());
                  continue;
              }
              if(p.getName().equals("openid.return_to")){
                  return_to = p.getValue();
              }
          }
          return id;
    }
    
    private String findReturnTo(IHttpRequestResponse message){
        IRequestInfo iri = super.getCallbacks().getHelpers().analyzeRequest(message);
          List<IParameter> list = iri.getParameters();
          String returnTo = null;
          for(IParameter p : list){
              if(p.getName().equals("openid.return_to")){
                  returnTo = p.getValue();
                  break;
              }
          }
          return returnTo;
    }


    @Override
    public int analyseProtocol() {
        logging.log(getClass(), "\nAnalyse: "+getProtocol()+" with ID: "+getToken(), Logging.DEBUG);
        ArrayList<SSOProtocol> last_protocolflow = SSOProtocol.getLastProtocolFlow();
        if(last_protocolflow != null){
            double listsize = (double) last_protocolflow.size();
            double protocol = 0;
            double token = 0;
            double traffic = 0;
            for(SSOProtocol sso : last_protocolflow){
                if(sso.getProtocol().substring(0, 5).equals(this.getProtocol().substring(0, 5))){
                    logging.log(getClass(), sso.getProtocol(), Logging.DEBUG);
                    protocol++;
                }
                if(sso.getToken().equals(this.getToken())){
                    logging.log(getClass(),sso.getToken(), Logging.DEBUG);
                    token++;
                }
                String returnTo = findReturnTo(sso.getMessage());
                if(returnTo != null){
                    if(return_to.equals(returnTo)){
                        logging.log(getClass(),returnTo, Logging.DEBUG);
                        traffic++;
                    }
                }
                
            }
            
            if(listsize >= 0){
                double prob = ((protocol/listsize)+(token/listsize)+(traffic/listsize))/3;
                logging.log(getClass(),"Probability: "+prob, Logging.DEBUG);
                if(prob >= 0.7){
                    return getIDOfLastList();
                }
            }
            
        }
        return newProtocolflowID();
    }

}
