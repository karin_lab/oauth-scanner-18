
package Scan;

import burp.IBurpExtenderCallbacks;
import burp.IHttpRequestResponse;
import burp.IParameter;
import burp.IRequestInfo;
import GUI.Logging;
import static Scan.OAuth.ID;
import static table.SSOProtocol.getIDOfLastList;
import static table.SSOProtocol.newProtocolflowID;
import table.SSOProtocol;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OpenIDConnect extends SSOProtocol{

    public static final String NAME = "OpenID Connect";

    
    public OpenIDConnect(IHttpRequestResponse message, String protocol, IBurpExtenderCallbacks callbacks){
        super(message, protocol, callbacks);
        super.setToken(findToken());
        super.setProtocolflowID(analyseProtocol());
        add(this, getProtocolflowID());
    }
    
  
    @Override
    public String decode(String input) {
        if(Encoding.isURLEncoded(input)){
            return super.getCallbacks().getHelpers().urlDecode(input);
        }
        return input;
    }

   
    @Override
    public String findToken() {
        IRequestInfo iri = super.getCallbacks().getHelpers().analyzeRequest(super.getMessage());
        List<IParameter> list = iri.getParameters();
        String id = "Not Found!";
        for(IParameter p : list){
            if(p.getName().equals("state")){
                id = p.getValue();
                continue;
            }
//            if(p.getName().equals("openid.identity")){
//                id = decode(p.getValue());
//                continue;
//            }
            if(p.getName().equals(ID)){
                id = p.getValue();
            }
        }
        if(id.equals("Not Found!")){
            String response = getHelpers().bytesToString(getMessage().getResponse());
            Pattern pat = Pattern.compile("client_id=(.*?)\\\\u0026");
            Matcher m = pat.matcher(response);
            if(m.find()){
                id = m.group(1);
            }
        }
        if(id.equals("Not Found!")){
            String request = getHelpers().bytesToString(getMessage().getRequest());
            Pattern pat = Pattern.compile("state=(.*?)&");
            Matcher m = pat.matcher(request);
            if(m.find()){
                id = m.group(1);
            }
        }
        return id;
    }


    @Override
    public int analyseProtocol() {
        logging.log(getClass(),"\nAnalyse: "+getProtocol()+" with ID: "+getToken(), Logging.DEBUG);
        ArrayList<SSOProtocol> last_protocolflow = SSOProtocol.getLastProtocolFlow();
        if(last_protocolflow != null){
            double listsize = (double) last_protocolflow.size();
            double protocol = 0;
            double token = 0;
            
            long tmp = 0;
            long curr_time = 0;
            long last_time = 0;
            boolean wait = true;
            
            for(SSOProtocol sso : last_protocolflow){
                if(sso.getProtocol().substring(0, 4).equals(this.getProtocol().substring(0, 4))){
                    logging.log(getClass(),sso.getProtocol(), Logging.DEBUG);
                    protocol++;
                }
                if(sso.getToken().equals(this.getToken())){
                    logging.log(getClass(),sso.getToken(), Logging.DEBUG);
                    token++;
                }
                if(wait){
                    wait = false;
                } else {
                    curr_time = sso.getTimestamp();
                    tmp += curr_time-last_time;
                    logging.log(getClass(),"Diff: "+(curr_time-last_time), Logging.DEBUG);
                }
                last_time = sso.getTimestamp();
            }
            
            if(listsize >= 0){
                double diff_time = ((double)tmp/listsize);
                double curr_diff_time = getTimestamp() - last_protocolflow.get(last_protocolflow.size()-1).getTimestamp();
                double time_bonus = 0;
                logging.log(getClass(),"CurrDiff:"+curr_diff_time+" Diff:"+diff_time, Logging.DEBUG);
                if(curr_diff_time <= (diff_time+4000)){
                    time_bonus = 0.35;
                }
                double prob = ((protocol/listsize)+(token/listsize)*2)/3+(time_bonus);
                logging.log(getClass(),"Probability: "+prob, Logging.DEBUG);
                if(prob >= 0.7){
                    return getIDOfLastList();
                }
            }
            
        }
        return newProtocolflowID();
    }
    
}
