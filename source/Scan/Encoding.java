
package Scan;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public abstract class Encoding {

   
    public static final int URL_ENCODED = 1;

   
    public static final int BASE64_ENCODED = 2;

    
    public static final int DEFLATED = 3;
    
    
    public static int getEncoding(String data){
        
        if(isURLEncoded(data)){
            return URL_ENCODED;
        } else if(isBase64Encoded(data)){
            return BASE64_ENCODED;
        } else try {
            if(isDeflated(data.getBytes("ASCII"))){
                return DEFLATED;
            }
        } catch (IOException ex) {
            Logger.getLogger(Encoding.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
 
    public static boolean isURLEncoded(String data){
        boolean flag = true;
        //filter non ASCII chars
        if(!regex_contains("([^\\x00-\\x7F])", data)){
            try {
                String tmp = URLDecoder.decode(data, "ASCII");
                if(tmp.equals(data)){
                    return false;
                }
            } catch (UnsupportedEncodingException ex) {
                flag = false;
            }
        } else {
            flag = false;
        }
        String pattern = "(%[a-zA-Z0-9]{2})";
        return (flag && regex_contains(pattern, data));
    }
    
    
    public static boolean isBase64Encoded(String data){
        String pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        return regex_contains(pattern, data);
    }
    
    
    public static boolean isDeflated(byte[] data) throws IOException{
        try{
            Compression.decompress(data);
        } catch(DataFormatException e){
            return false;   
        }
        return true; 
    }
    
    
    public static boolean isJSON(String data){
        JSONParser parser = new JSONParser();
        try{
            JSONObject json = (JSONObject) parser.parse(data);
        } catch(ParseException e){
            return false;
        }
        return true;
    }
    
    
    public static boolean isJWT(String data){
        String[] base64 = data.split("\\.");
        boolean val = false;
        try{
            val = isBase64Encoded(base64[0]) && isBase64Encoded(base64[1]) && isBase64Encoded(base64[2]);
        } catch(IndexOutOfBoundsException e){
            return false;
        }
        return val;
    }
    
    private static boolean regex_contains(String pattern, String data){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(data);
        return m.find();
    }
}
