package Scan;

import burp.IBurpExtenderCallbacks;
import burp.IExtensionHelpers;
import burp.IHttpListener;
import burp.IHttpRequestResponse;
import burp.IParameter;
import burp.IRequestInfo;
import burp.IResponseInfo;
import GUI.UIOptions;
import GUI.Logging;
import static Scan.ParameterUtilities.getFirstParameterByName;
import static Scan.ParameterUtilities.parameterListContainsParameterName;
import burp.IHttpService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import table.SSOProtocol;
import table.Table;
import table.TableDB;
import table.TableEntry;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import static j2html.TagCreator.*;


public class ScanAndMarkSSO implements IHttpListener {
    
        private IHttpRequestResponse prev_message = null;
        private boolean oauth_code_requested = false;
    
        private static int counter = 1;
        
	private static final Set<String> IN_REQUEST_OAUTH_PARAMETER = new HashSet<String>(Arrays.asList(
            new String[]{"redirect_uri", "scope", "client_id", "client_secret",  "response_type"}
	));
        private static final Set<String> IN_REQUEST_OAUTH_AUTH_CODE_GRANT_PARAMETER = new HashSet<String>(Arrays.asList(
            new String[]{"grant_type", "response_type"}
	));
        private static final Set<String> IN_REQUEST_OAUTH_IMPLICIT_PARAMETER = new HashSet<String>(Arrays.asList(
            new String[]{"access_token", "response_type"}
	));
        
        private static final Set<String> IN_REQUEST_FACEBOOKCONNECT_PARAMETER = new HashSet<String>(Arrays.asList(
            new String[]{"app_id", "domain", "origin",  "sdk"}
	));
        
        //Monolitic protocol hosts
        private final String FBC_HOST = "facebook.com";
        private final String MSA_HOST = "live.com";
        private final String MSA_HOST2 = "live.net";
        private final String MSA_HOST3 = "contoso.com";
        private final String BID_HOST = "persona.org";

	private static final String HIGHLIGHT_COLOR = "green";
	private static final String MIMETYPE_HTML = "HTML";
	private static final int STATUS_OK = 200;

	private IBurpExtenderCallbacks callbacks;
	private IExtensionHelpers helpers;
        
        public String dest ;
         public String dest2 ;
        public boolean file_exist = false; 
        public String type_oauth = "";
        
        public String grant;
        public long Classify_millis;
       public long millis;
       public long current_millis;
       public long Vul_millis;
       
     //public Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       
       public String Possible_Threat;
       public double severe_level;
       
       
	public ScanAndMarkSSO(IBurpExtenderCallbacks callbacks) {
            this.callbacks = callbacks;
            this.helpers = callbacks.getHelpers();
	}
        
       
	@Override
	public void processHttpMessage(int toolFlag, boolean isRequest, IHttpRequestResponse httpRequestResponse) {
            // only flag messages sent/received by the proxy
            if (toolFlag == IBurpExtenderCallbacks.TOOL_PROXY && !isRequest) {
                    TableEntry entry = processSSOScan(httpRequestResponse);// ตรงนี้จะไปเช็ค ใน option ว่าเปิดยุป่าวแล้วก็สแกน ไรเลย ต้องตามลำดับ
                    if(entry != null){
                        updateTables(entry);
                    }
                    
                    processLoginPossibilities(httpRequestResponse);// อันนี้เป็นอันแรก เพราะ ทุกอันต้องใช้  prev_message
                    prev_message = httpRequestResponse;
            }
	}

	private void processLoginPossibilities(IHttpRequestResponse httpRequestResponse) {
            final byte[] responseBytes = httpRequestResponse.getResponse();
            IResponseInfo responseInfo = helpers.analyzeResponse(responseBytes);
            checkRequestForOpenIdLoginMetadata(responseInfo, httpRequestResponse);
	}

	private TableEntry processSSOScan(IHttpRequestResponse httpRequestResponse) {
            IRequestInfo requestInfo = helpers.analyzeRequest(httpRequestResponse);
            //String host = requestInfo.getUrl().getHost();
            //The order is very important!
         
            if(UIOptions.isOAuthActive()){
                //Exclude all monolithic protocols
               // if(!host.contains(FBC_HOST) || !host.contains(MSA_HOST) || !host.contains(BID_HOST)){
                    SSOProtocol protocol = checkRequestForOAuth(requestInfo, httpRequestResponse);
                    if(protocol != null){
                        protocol.setCounter(counter++);
                        return protocol.toTableEntry();
                    }
                //}
            }
          
            return null;
	}
        
        private void updateTables(TableEntry entry){
            //Full history
            TableDB.getTable(0).getTableHelper().addRow(entry);
            //Add content to additional tables
            for(int i = 1; i<TableDB.size(); i++){
                Table t = TableDB.getTable(i);
                t.update();
            }
        }
        
       
        
	private SSOProtocol checkRequestForOAuth(IRequestInfo requestInfo, IHttpRequestResponse httpRequestResponse) {
            
            OAuth oauth = null;
            String comment = null;
            grant =null;
            
            if (parameterListContainsParameterName(requestInfo.getParameters(), IN_REQUEST_OAUTH_PARAMETER)) {
                oauth =  new OAuth(httpRequestResponse, "OAuth", callbacks);
               
                
                //Code Flow
                if(parameterListContainsParameterName(requestInfo.getParameters(), IN_REQUEST_OAUTH_AUTH_CODE_GRANT_PARAMETER)){
                    
                    if(null != prev_message){
                        IResponseInfo prev_responseInfo = helpers.analyzeResponse(prev_message.getResponse());
                        //Check for OAuth Authorization Code Grant Request
                        
                        
                        
                        if(prev_responseInfo.getStatusCode() == 302){
                            IParameter response_type = helpers.getRequestParameter(httpRequestResponse.getRequest(), "response_type");
                            IParameter pre_response_type = helpers.getRequestParameter(prev_message.getRequest(), "response_type");
                            if(response_type != null && pre_response_type != null){
                                if(response_type.getValue().contains("code") && pre_response_type.getValue().contains("code")){
                                    comment = "OAuth ACG Request";
                                    oauth_code_requested = true;
                                } else {

                                    // Check for OAuth Authorization Code Grant Code
                                    IParameter code = helpers.getRequestParameter(httpRequestResponse.getRequest(), "code");
                                    if(code != null && oauth_code_requested && comment == null){
                                        comment = "OAuth ACG Code";
                                        oauth_code_requested = false;
                                    }

                                    //Check for OAuth Authorization Code Grant Token Request
                                    IParameter grant_type = helpers.getRequestParameter(httpRequestResponse.getRequest(), "grant_type");
                                    if(grant_type != null && comment == null){
                                        if(grant_type.getValue().contains("auth_code")){
                                            comment = "OAuth ACG Token Request";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                //Implicit Flow     
                } else if(parameterListContainsParameterName(requestInfo.getParameters(), IN_REQUEST_OAUTH_IMPLICIT_PARAMETER) && comment == null){
                    if(null != prev_message){
                        IResponseInfo prev_responseInfo = helpers.analyzeResponse(prev_message.getResponse());
                        //Check for OAuth Implicit Grant Request
                        if(prev_responseInfo.getStatusCode() == 302){
                            IParameter response_type = helpers.getRequestParameter(httpRequestResponse.getRequest(), "response_type");
                            IParameter pre_response_type = helpers.getRequestParameter(prev_message.getRequest(), "response_type");
                            
                            if(response_type.getValue().contains("token") && pre_response_type.getValue().contains("token")){
                                comment = "OAuth Implicit Grant Request";
                                oauth_code_requested = true;
                            }
                        }
                    }
                    
                    // Check for OAuth Implicit Token
                    if(helpers.analyzeResponse(httpRequestResponse.getResponse()).getStatusCode() == 302 && comment == null){
                        String response = helpers.bytesToString(httpRequestResponse.getResponse());
                        // Check for OAuth Implicit Token
                        Pattern p = Pattern.compile("Location:.*?#.*?access_token=.*?&?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher m = p.matcher(response);
                        if(m.find()){
                            comment = "OAuth Implicit Token";
                            oauth_code_requested = false;
                        }
                    } else if(comment == null){
                        comment = "OAuth (IF)";
                    }
                }
                
                 if(comment == null){
                    //Check for other OAuth flows
                    IParameter grant_type = helpers.getRequestParameter(httpRequestResponse.getRequest(), "grant_type");
                    if(grant_type != null){
                        switch(grant_type.getValue()){
                            case "authorization_code":
                                comment = "OAuth Access Token Request";
                                break;
                            case "refresh_token":
                                comment = "OAuth Refresh Token Request";
                                break;
                            case "password":
                                comment = "OAuth Resource Owner Password Credentials Grant";
                                break;
                            case "client_credentials":
                                comment = "OAuth Client Credentials Grant";
                                break;
                            case "urn:ietf:params:oauth:grant-type:jwt-bearer":
                                comment = "OAuth Extension JWT Grant";
                                break;
                            case "urn:oasis:names:tc:SAML:2.0:cm:bearer":
                                comment = "OAuth Extension SAML Grant";
                                break;
                            default:
                                comment = "OAuth ACGF";
                        }
                    }
                }
               
                
                if(comment == null){
                    type_oauth =  "OAuth";
                }
               
                oauth.setType(type_oauth);
                markRequestResponse(httpRequestResponse, comment, "green");
                 //check vulnerabilities
               
                 IParameter response_type = helpers.getRequestParameter(httpRequestResponse.getRequest(), "response_type");
                 if(response_type != null ){
                                if(response_type.getValue().contains("code") ){
                                    type_oauth=grant =  "Authorization Code Grant";
                                    
                                } 
                                if(response_type.getValue().contains("token")){
                                type_oauth=grant =  "Implicit Grant";
                                
                            }
                             oauth.setType(type_oauth);
                 }      
                        
                         
                       
                        
               
                                
                
                if(oauth != null){
                      millis = System.currentTimeMillis();
                     String response = helpers.bytesToString(httpRequestResponse.getResponse());
                     
                    
                     Pattern p4=  Pattern.compile("X-Content-Type-Options: nosniff", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                         Matcher m4 = p4.matcher(response);
                         if(!m4.find()){
                             Vul_millis = System.currentTimeMillis();
                             //long Scan1 =  Scan1_test-millis;
                            comment = "MiME SNiFFinG";
                             ReportScanText(httpRequestResponse, comment,1,grant);
                          //  markRequestResponse(httpRequestResponse, comment, "yellow", Vul_millis);
                       markRequestResponse(httpRequestResponse, comment, "yellow");
                        }
                         Pattern p2 = Pattern.compile("X-XSS-Protection: 0", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                         Matcher m2 = p2.matcher(response);
                         if(m2.find()){
                           Vul_millis = System.currentTimeMillis();
                            comment = "XSS Token";
                             ReportScanText(httpRequestResponse, comment,3,grant);
                            //markRequestResponse(httpRequestResponse, comment, "yellow", Vul_millis);
                           markRequestResponse(httpRequestResponse, comment, "yellow");
                         }
                         
                         Pattern p3 = Pattern.compile("X-Frame-Options:", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                           Matcher m3 = p3.matcher(response);
                         if(!m3.find()){
                             Vul_millis = System.currentTimeMillis();
                            comment = "X-Frame Options header";
                             ReportScanText(httpRequestResponse, comment,4,grant);
                            //markRequestResponse(httpRequestResponse, comment, "yellow",Vul_millis);
                            markRequestResponse(httpRequestResponse, comment, "yellow");
                            
                           
                        }
                         
                 if(helpers.analyzeResponse(httpRequestResponse.getResponse()).getStatusCode() >= 300 && helpers.analyzeResponse(httpRequestResponse.getResponse()).getStatusCode() <= 308){
                       
                        Pattern p1 = Pattern.compile("Location: https:.*", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                     
                        Matcher m1 = p1.matcher(response);
                        if(!m1.find()){
                           Vul_millis = System.currentTimeMillis();
                            comment = "HTTPS for Redirect URI";
                            ReportScanText(httpRequestResponse, comment,2,grant);
                            //markRequestResponse(httpRequestResponse, comment, "yellow",Vul_millis);
                            markRequestResponse(httpRequestResponse, comment, "yellow");
                        }
                        
                        
                         
                         
                        
                         
                     
                  IParameter response_check_client = helpers.getRequestParameter(httpRequestResponse.getRequest(), "client_id");
                  IParameter response_check_client2 = helpers.getRequestParameter(httpRequestResponse.getRequest(), "clientid");
                  
                 
                     IParameter state = helpers.getRequestParameter(httpRequestResponse.getRequest(), "state");
                    
                            if(state == null  ){
                            Vul_millis = System.currentTimeMillis();
                                    comment = "State-parameter";
                                    ReportScanText(httpRequestResponse, comment,6,grant);
                                   // markRequestResponse(httpRequestResponse, comment, "orange",Vul_millis);
                                    markRequestResponse(httpRequestResponse, comment, "orange");
                            }
                     if(response_check_client == null && response_check_client2 == null){
                       Vul_millis = System.currentTimeMillis();
                        comment = "Client_ID";
                        ReportScanText(httpRequestResponse, comment,5,grant);
                        //markRequestResponse(httpRequestResponse, comment, "red",Vul_millis);
                        markRequestResponse(httpRequestResponse, comment, "red");
                    }
                 }
              if(httpRequestResponse.getComment()==null){
                                     comment = "N/A";  
                                     markRequestResponse(httpRequestResponse, comment, "green");  }
                
                }
            }
            //check acces token using
           // String host = requestInfo.getUrl().getHost();
             String request = helpers.bytesToString(httpRequestResponse.getRequest());   
             
                        //access_token
                        Pattern p6=  Pattern.compile(".*accessToken", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher m6 = p6.matcher(request);
                        
                       
                        
                        Pattern p8 = Pattern.compile(".*access_token", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher m8 = p8.matcher(request);
                       
                        Pattern p9=  Pattern.compile("POST", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher m9 = p9.matcher(request);
                       
                         if(m6.find() || m8.find()){
                            
                             oauth =  new OAuth(httpRequestResponse, "OAuth", callbacks);
                           type_oauth =  "Oauth token";
                          
                           oauth.setType(type_oauth);
                           httpRequestResponse.setHighlight("green");
                           //long hightlight_token =  System.currentTimeMillis();
                                   // markRequestResponse(httpRequestResponse, comment, "green",hightlight_token);
                                   
                     
                        Pattern XSRF=  Pattern.compile("X-XSRF-Token:", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher M_XSRF = XSRF.matcher(request);
                        Pattern XSRF2=  Pattern.compile("X-CSRFToken:", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                        Matcher M_XSRF2 = XSRF2.matcher(request);
                        
                                 if(m9.find()){
                           
                        
                                      if(!M_XSRF.find()&&!M_XSRF2.find()){
                            Vul_millis = System.currentTimeMillis();
                                    comment = "CSRF-Token";
                                    ReportScanText(httpRequestResponse, comment,7,grant);
                                  //  markRequestResponse(httpRequestResponse, comment, "orange",Vul_millis);
                                   markRequestResponse(httpRequestResponse, comment, "orange");
                                      }
                        } 
                                 
                                 if(httpRequestResponse.getComment()==null){
                                     comment = "N/A";  
                                    // markRequestResponse(httpRequestResponse, comment, "green",hightlight_token);  
                                  markRequestResponse(httpRequestResponse, comment, "green"); 
                                 }
                         }
                          Classify_millis = System.currentTimeMillis();
                long Classify_test = Classify_millis-millis;
                    //  markRequestResponse(httpRequestResponse, "Vul time = "+ Classify_test,"green");
            return oauth;
	}
        

	private boolean checkRequestForOpenIdLoginMetadata(IResponseInfo responseInfo, IHttpRequestResponse httpRequestResponse) {
            if (responseInfo.getStatusCode() == STATUS_OK && MIMETYPE_HTML.equals(responseInfo.getStatedMimeType())) {
                final byte[] responseBytes = httpRequestResponse.getResponse();
                final int bodyOffset = responseInfo.getBodyOffset();
		final String responseBody = (new String(responseBytes)).substring(bodyOffset);
                final String response = helpers.bytesToString(responseBytes);
                final String request = helpers.bytesToString(httpRequestResponse.getResponse());
                
                Pattern p = Pattern.compile("=[\"'][^\"']*openid[^\"']*[\"']", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(responseBody);
                if (m.find()) {
                    markRequestResponse(httpRequestResponse, "OpenID Login Possibility", "green");
                    IRequestInfo iri = helpers.analyzeRequest(httpRequestResponse);
                    callbacks.issueAlert("OpenID Login on: "+iri.getUrl().toString()); 
                    return true;
                }
                p = Pattern.compile("rel=\"openid(.server|.delegate|2.provider|2.local_id)\"", Pattern.CASE_INSENSITIVE);
                m = p.matcher(responseBody);
                if (m.find()) {
                    markRequestResponse(httpRequestResponse, "OpenID Metadata", "green");
                    IRequestInfo iri = helpers.analyzeRequest(httpRequestResponse);
                    callbacks.issueAlert("OpenID Login on: "+iri.getUrl().toString());
                    return true;
                }
                p = Pattern.compile("openid(.server|.delegate|2.provider|2.local_id|.click)", Pattern.CASE_INSENSITIVE);
                m = p.matcher(request);
                if (m.find()) {
                    markRequestResponse(httpRequestResponse, "OpenID Metadata", "green");
                    IRequestInfo iri = helpers.analyzeRequest(httpRequestResponse);
                    callbacks.issueAlert("OpenID Login on: "+iri.getUrl().toString());
                    return true;
                }
                p = Pattern.compile("X-XRDS-Location:\\s(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})", Pattern.CASE_INSENSITIVE);
                m = p.matcher(response);
                if (m.find()) {
                    markRequestResponse(httpRequestResponse, "OpenID Metadata", "green");
                    IRequestInfo iri = helpers.analyzeRequest(httpRequestResponse);
                    callbacks.issueAlert("OpenID Login on: "+iri.getUrl().toString()); 
                    return true;
                }
                p = Pattern.compile("xmlns:xrds=\"xri://$xrds\" xmlns=\"xri://$xrd*($v*2.0)\"", Pattern.CASE_INSENSITIVE);
                m = p.matcher(response);
                if (m.find()) {
                    markRequestResponse(httpRequestResponse, "OpenID Metadata", "green");
                    IRequestInfo iri = helpers.analyzeRequest(httpRequestResponse);
                    callbacks.issueAlert("OpenID Login on: "+iri.getUrl().toString()); 
                    return true;
                }
            }
            return false;
	}
        
	
        private void markRequestResponse(IHttpRequestResponse httpRequestResponse, String message, String colour) {
            if(UIOptions.isHighlighted()){
                httpRequestResponse.setHighlight(colour);
            }
            final String oldComment = httpRequestResponse.getComment();
            if (oldComment != null && !oldComment.isEmpty()) {
                    httpRequestResponse.setComment(String.format("%s, %s", oldComment, message));
            } else {
                    httpRequestResponse.setComment(message);
            }
            Logging.getInstance().log(getClass(), message, Logging.DEBUG);
	}
        
        
        
         private void markRequestResponse(IHttpRequestResponse httpRequestResponse, String message, String colour,long test) {
            if(UIOptions.isHighlighted()){
                httpRequestResponse.setHighlight(colour);
            }
            final String oldComment = httpRequestResponse.getComment();
            long high_millis = System.currentTimeMillis() - test;
            if (oldComment != null && !oldComment.isEmpty()) {
                    httpRequestResponse.setComment(String.format("%s, %s"+" |hightlight"+high_millis+"|", oldComment, message));
            } else {
                    httpRequestResponse.setComment(message+" |hightlight,"+high_millis+"|");
            }
            Logging.getInstance().log(getClass(), message, Logging.DEBUG);
	}
         
        /*private void markRequestResponse_type(IHttpRequestResponse httpRequestResponse, String message, String colour) {
            if(UIOptions.isHighlighted()){
                httpRequestResponse.setHighlight(colour);
            }
            
            final String oldComment = httpRequestResponse.getComment();
            
            if (oldComment != null && !oldComment.isEmpty()) {
                    httpRequestResponse.setComment(String.format("%s, %s", oldComment, message));
            } else {
                    httpRequestResponse.setComment(message);
            }
            Logging.getInstance().log(getClass(), message, Logging.DEBUG);
	}*/
    
        public void ReportScanText(IHttpRequestResponse httpRequestResponse,String comment,int i,String gra){
                String target;
                File file;
                Path path ;
               
                if(file_exist==false){
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                    
                    dest = "Result/Report_"+timeStamp+".txt";
                    target = "Result";
                    file = new File(dest);
                    path = Paths.get(target);
             if(Files.notExists(path)){
        try {
            Files.createFile(Files.createDirectories(path)).toFile();
        } catch (IOException ex) {
            Logger.getLogger(UIOptions.class.getName()).log(Level.SEVERE, null, ex);
        }
             
             }
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch(Exception ex) {
                Logging.getInstance().log(getClass(), ex);
            }
        } 
                }
                file_exist=true;
                 Generate_report(httpRequestResponse,comment,dest,i,gra);
        }
        
        public void Generate_report(IHttpRequestResponse httpRequestResponse,String comment,String dest ,int i,String gra){
       
            File file;
           
             file = new File(dest);
            
              FileWriter writer;
             
        IHttpService httpService = httpRequestResponse.getHttpService();
        
                        switch(i){
                            case 1:
                                  severe_level=6.1;
                                break;
                            case 2: 
                                severe_level=6.1;
                                break;
                            case 3:                             
                                severe_level=6.1;
                                break;
                            case 4:
                                severe_level=4.7;
                                break;
                            case 6:                         
                                severe_level=7.8;
                                break;
                            case 5:                              
                                severe_level=9.8;
                                break;
                            case 7:                               
                                severe_level=8.8;
                                break;
                            default:
                                
                        }
                    
        
                  try {
                      writer = new FileWriter(file, true);  //True = Append to file, false = Overwrite
                      String host = httpService.getHost();
                     
                      IRequestInfo info = helpers.analyzeRequest(httpRequestResponse);
                      String url = info.getUrl().toString();
                    //   current_millis = System.currentTimeMillis();
                   //  long testcase = current_millis-test;
                      writer.write(host +","+url+","+ comment+","+severe_level+","+gra+"\r\n");
                    writer.close();
                  } catch (IOException ex) {
                      Logger.getLogger(ScanAndMarkSSO.class.getName()).log(Level.SEVERE, null, ex);
                  }
                  
          
    }
     
        
}