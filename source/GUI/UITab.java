
package GUI;

import burp.IBurpExtenderCallbacks;
import burp.ITab;
import java.awt.Component;

/**
 * An additional tab in Burp Suite
 * @author Tim Guenther
 * @version 1.0
 */
public class UITab implements ITab {
    
    //public UIPanel panel;
    private UIMain main;
    private final IBurpExtenderCallbacks callbacks;
    
    /**
     * Create a new Tab.
     * @param callbacks {@link burp.IBurpExtenderCallbacks}
     */
    public UITab(IBurpExtenderCallbacks callbacks) {
        this.callbacks = callbacks;
        this.main = new UIMain(callbacks);
        callbacks.customizeUiComponent(main);
        callbacks.addSuiteTab(this);
    }
    
    /**
     * 
     * @return Get the UI component that should be registered at the Burp Suite GUI. 
     */
    @Override
    public Component getUiComponent() {
        return main;
    }
    
    /**
     * 
     * @return Get the UI component that should be registered at the Burp Suite GUI.
     */
    public UIMain getUiMain(){
        return main;
    }
    
    /**
     * 
     * @return Get the Headline for the Tab. 
     */
    @Override
    public String getTabCaption() {
        return "Overscan";
    }
}
